


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class FixMyBugs {

	@Test
	public void bug() {
		try {
			// launch the browser
			System.setProperty("webdriver.chrome.driver","./Drivers/chromedriver.exe");
			ChromeOptions options=new ChromeOptions();
			
			options.addArguments("--disable-notifications");
			ChromeDriver driver = new ChromeDriver(options);
			driver.get("https://www.myntra.com/");
			driver.manage().window().maximize();


			// Mouse Over on Men
			Actions builder = new Actions(driver);

			builder.moveToElement(driver.findElementByLinkText("Men")).perform();
			//WebElement menLink = driver.findElementByLinkText("Men");
			//builder.clickAndHold(menLink).perform();
			//builder.contextClick(menLink).perform();

			// Click on Jackets
			driver.findElementByLinkText("Jackets").click();


			/*// Find the count of Jackets
						String leftCount = 
					driver.findElementByXPath("//div[@class='horizontal-filters-sortContainer").getText().replaceAll("[^0-9]", "");
			System.out.println(leftCount);*/
			/*
			String leftCount = 
					driver.findElementByXPath("//ul[@class='categories-list']").getText().replaceAll("[^0-9]", "");
			System.out.println(leftCount);
			 */


			// Click on Jackets and confirm the count is same
			String jackets = driver.findElementByXPath("//label[text()='Jackets']").getText().replaceAll("[^0-9]", "");
			System.out.println("The total count of Jackets is " +jackets);
			String rainJackets = driver.findElementByXPath("//label[text()='Rain Jacket']").getText().replaceAll("[^0-9]", "");
			System.out.println("The total count of Jackets is " +rainJackets);
			int count = Integer.parseInt(jackets) + Integer.parseInt(rainJackets);
			System.out.println("The total count of jackets is " +count);

			// Wait for some time
			Thread.sleep(5000);

			// Check the count
			String rightCount = 
					driver.findElementByXPath("//div[@class='horizontal-filters-sortContainer']")
					.getText()
					.replaceAll("[^0-9]", "");
			System.out.println("Total Men Jackets count in right side is " + rightCount);
			int menCount = Integer.parseInt(rightCount);


			// If both count matches, say success
			if(count == menCount) {
				System.out.println("The count matches on either case");
			}else {
				System.err.println("The count does not match");
			}

			// Click on Offers
			driver.findElementByXPath("//h4[text()='Offers']").click();

			// Find the costliest Jacket
			
			
			List<WebElement> productsPrice = driver.findElementsByXPath("//span[@class='product-discountedPrice']");
			List<String> price = new ArrayList<String>();

			for (WebElement eachPrice : productsPrice) {
				System.out.println("The list of price is " +eachPrice.getText().replaceAll("[^a-zA-Z0-9]", " "));
				price.add(eachPrice.getText().replaceAll("[^0-9]", ""));
			}

			// Sort them 
			/*Collections.sort(price);
			for (String maxPrice : price) {
				System.out.println("Maximum " + maxPrice);
			}
			int size = price.size();
			String string = price.get(size-1);
			System.out.println(string);
			*/
			String maxPrice = Collections.max(price);	
			System.out.println("The maximum value is " + maxPrice);
			//driver.close();

			// Print Only Allen Solly Brand Minimum Price
			driver.findElementByXPath("//div[@class='brand-more']").click();
			driver.findElementByXPath("//label[text()='Allen Solly']").isSelected();
			/*driver.findElementByXPath("//input[@value='Allen Solly']/following:sibling::div").click();

			// Find the costliest Jacket
			List<WebElement> allenSollyPrices = driver.findElementsByXPath("//span[@class='product-discountedPrice']");

			onlyPrice = new ArrayList<String>();

			for (WebElement productPrice : productsPrice) {
				onlyPrice.add(productPrice.getText().replaceAll("//D", ""));
			}

			// Get the minimum Price 
			String min = Collections.min(onlyPrice);

			// Find the lowest priced Allen Solly
			System.out.println(min);
*/		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


	}

 }


