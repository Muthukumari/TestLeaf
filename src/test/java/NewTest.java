import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.apache.poi.util.SystemOutLogger;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

public class NewTest {
  @Test
  public void f() {
	  System.out.println("@Test is displayed");
  }
  @BeforeMethod
  public void beforeMethod() {
	  System.out.println("@BeforeMethod is displayed");
  }

  @AfterMethod
  public void afterMethod() {
	  System.out.println("@AfterMethod is displayed");
  }

  @BeforeClass
  public void beforeClass() {
	  System.out.println("@BeforeClass is displayed");
  }

  @AfterClass
  public void afterClass() {
	  System.out.println("@AfterClass is displayed");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("@BeforeTest is displayed");
  }

  @AfterTest
  public void afterTest() {
	  System.out.println("@AfterTest is displayed");
  }

  @BeforeSuite
  public void beforeSuite() {
	  System.out.println("@BeforeSuite is displayed");
  }

  @AfterSuite
  public void afterSuite() {
	  System.out.println("@AfterSuite is displayed");
  }

}
