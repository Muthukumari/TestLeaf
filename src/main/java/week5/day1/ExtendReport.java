package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendReport {

	public static void main(String[] args) throws IOException {
		//To create a html report
		ExtentHtmlReporter report = new ExtentHtmlReporter("./reports/results.html");
		
		//to maintain history
		report.setAppendExisting(true);
		
		//to convert into editable mode
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(report);
		
		//Test Case information
		ExtentTest test = extent.createTest("Create", "Create Lead test case");
		test.assignAuthor("Muthukumari");
		test.assignCategory("Regression");
		
		//Pass and Fail assignment
		test.pass("The DemoSalesManager is entered successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./../Snaps/img.png").build());
		test.fail("The password is not entered properly");
		
		//manadatory action to flush the html page
		extent.flush();
		
	}

}
