package wdMethods;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtendReportsMethod {
	public static ExtentReports extent;
	public static ExtentTest test;
	public String testCaseName,testCaseDescription,author,category,excelFileName;

	@BeforeSuite(groups = "common")
	public void html() {
	ExtentHtmlReporter report = new ExtentHtmlReporter("./reports/results.html");
	
	//to maintain history
	report.setAppendExisting(true);
	
	//to convert into editable mode
	extent = new ExtentReports();
	extent.attachReporter(report);
	}
	
	@BeforeMethod(groups = "common")
	
	public void results() {
		test = extent.createTest(testCaseName,testCaseDescription);
		test.assignAuthor(author);
		test.assignCategory(category);
		
	}
	
	public void testSteps(String desc , String Status) {
		if(Status.equalsIgnoreCase("PASS")) {
			test.pass(desc);
		}if(Status.equalsIgnoreCase("FAIL")) {
			test.fail(Status);
		}
	}
	@AfterSuite(groups = "common")

	public void stopResult() {
		extent.flush();
	}
	}


