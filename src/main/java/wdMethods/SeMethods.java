package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeMethods extends ExtendReportsMethod implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;
	public void startApp(String browser, String url) {
		try {
			if(browser.equalsIgnoreCase("chrome")){
				System.setProperty("webdriver.chrome.driver", "./Drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} else if (browser.equalsIgnoreCase("firefox")){
				System.setProperty("webdriver.gecko.driver", "./Drivers/geckodriver.exe");
				driver = new FirefoxDriver();
			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			System.out.println("The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			System.err.println("The Browser "+browser+" not Launched");
		} finally {
			takeSnap();
		}

	}



	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id"	 : return driver.findElementById(locValue);
			case "class" : return driver.findElementByClassName(locValue);
			case "xpath" : return driver.findElementByXPath(locValue);
			case "LinkText" : return driver.findElementByLinkText(locValue); 
			case "name" : return driver.findElementByName(locValue);
			}
		} catch (NoSuchElementException e) {
			System.err.println("The Element is not found");
		} catch (Exception e) {
			System.err.println("Unknow Exception ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		// TODO Auto-generated method stub
		return null;
	}


	public void type(WebElement ele, String data) {
		try {
			ele.sendKeys(data);
			testSteps("The data "+data+" is Entered Successfully", "PASS");
			System.out.println("The data "+data+" is Entered Successfully");
		} catch (WebDriverException e) {
			testSteps("The data "+data+" is Not Entered" , "FAIL");
			System.out.println("The data "+data+" is Not Entered");
		} finally {
			takeSnap();
		}
	}


	public void clickWithNoSnap(WebElement ele) {
		try {
			ele.click();
			testSteps("The Element "+ele+" Clicked Successfully", "PASS");
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (Exception e) {
			testSteps("The Element "+ele+" is not Clicked Successfully", "FAIL");
			System.err.println("The Element "+ele+"is not Clicked");
		}
	}



	@Override
	public void click(WebElement ele) {
		try {
			ele.click();
			testSteps("The Element "+ele+" Clicked Successfully", "PASS");
			System.out.println("The Element "+ele+" Clicked Successfully");
		} catch (WebDriverException e) {
			testSteps("The Element "+ele+"is not Clicked Successfully", "FAIL");
			System.err.println("The Element "+ele+"is not Clicked");
		} finally {
			takeSnap();
		}
	}

	@Override
	public String getText(WebElement ele) {
		String text = null;
		try {
			text = ele.getText();
			System.out.println("The captured text is "+text);
		} catch (Exception e) {
			System.out.println("Something went wrong");
		}
		finally {
			takeSnap();
		}
		return text
				;
	}

	
	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		try {
			Select dd = new Select(ele);
			dd.selectByVisibleText(value);
			System.out.println("The DropDown Is Selected with VisibleText "+value);
		} catch (Exception e) {
			System.err.println("The DropDown Is not Selected with VisibleText "+value);
		} finally {
			takeSnap();
		}

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		try {
			Select dd = new Select(ele);
			dd.selectByIndex(index);
			testSteps("The dropdown is selected using index " +index , "PASS");
			//System.out.println("The dropdown is selected using index " +index );
		} catch (Exception e) {
			System.out.println("The dropdown is not displayed properly "+index);
		}finally
		{
			takeSnap();
		}

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		try {
			String title = driver.getTitle(); 
			boolean equalsTitle = title.equals(expectedTitle);
			if(equalsTitle == true) {
				System.out.println("Both URLs are same");
			}
			else {
				System.out.println("Both URLs are different");
			}
		} catch (Exception e) {
			System.out.println("Something went worng");
		}
		return true;
	}


	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			boolean expectedValue = text.equals(expectedText);
			if(expectedValue == true) {
				System.out.println("The given value "+expectedValue+ " is verified successfully"); 
			}
			else {
				System.out.println("The given value "+expectedValue+ " is not verified successfully"); 
			}
		} catch (Exception e) {
			System.out.println("Something went wrong");
		}
	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		try {
			String text = ele.getText();
			boolean expectedValue = text.contains(expectedText);
			if(expectedValue == true) {
				System.out.println("The given value "+expectedValue+ " is verified successfully"); 
			}
			else {
				System.out.println("The given value "+expectedValue+ " is not verified successfully"); 
			}
		} catch (Exception e) {
			System.out.println("Something went wrong");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		try {
			String attributevalue = ele.getAttribute(attribute);
			if(attributevalue.equalsIgnoreCase(value))
			{
				System.out.println("Value is displayed");	
			}else {
				System.out.println("Value is not displayed");
			}
		} catch (Exception e) {
			System.out.println("Element is not displayed");
		}


	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		try {
			boolean selected = ele.isSelected();
			if(selected == true) {
			System.out.println("Element is selected");
			}else
			{
				System.out.println("Element is not selected");
			}
		} catch (Exception e) {
			System.out.println("Element is not found");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		try {
			ele.isDisplayed();
			System.out.println("Element is found");
		} catch (Exception e) {
			System.out.println("Element is not displayed");
		}finally
		{
			takeSnap();
		}

	}

	@Override
	public void switchToWindow(int index) {
		try {
			Set<String> moreWindow = driver.getWindowHandles();
			List<String> listOfWindows = new ArrayList<String>();
			listOfWindows.addAll(moreWindow);
			String newWindow = listOfWindows.get(index);
			driver.switchTo().window(newWindow);

		} catch (NoSuchWindowException e) {
			System.out.println("No windows are dislayed");	
		}
		takeSnap();

	}




	@Override
	public void switchToFrame(WebElement ele) {
		try {
			driver.switchTo().frame(ele);
			System.out.println("Switched to frame");
		} catch (NoSuchFrameException e) {
			System.out.println("Frane is not displayed");	
		}
		finally {
			takeSnap();
		}

	}

	@Override
	public void acceptAlert() {
		try {
			driver.switchTo().alert().accept();
			System.out.println("Alert is accepted");
		} catch (NoAlertPresentException e) {
			System.out.println("Alert is not displayed");
		}catch(UnhandledAlertException e) {
			System.out.println("Alert displayed but not handled");
		}

	}

	@Override
	public void dismissAlert() {
		try {
			driver.switchTo().alert().dismiss();
			System.out.println("The alert is dismissed");
		} catch (Exception e) {
			System.out.println("The alert is not dismissed");
		}

	}

	@Override
	public String getAlertText() {
		String text = null;
		try {
		  text = driver.switchTo().alert().getText();
			System.out.println("The getText is displayed");
		} catch (Exception e) {
			System.out.println("The get test is not displayed");
		}
		return text;
	}


	public void takeSnap() {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File des = new File("./Snaps/img"+i+".png");
		try {
			FileUtils.copyFile(src, des);
		} catch (IOException e) {
			System.err.println("IOException");
		}
		i++;
	}

	public void webDriverWait(WebElement ele)
	{
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(ele));
	}

	@Override
	public void closeBrowser() {
		driver.close();
	}

	@Override
	public void closeAllBrowsers() {
		driver.quit();
		System.out.println("All browsers are closed");

	}

}
