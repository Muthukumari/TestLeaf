package week4.day1.homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class DuplicateLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");;
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("//span[text() = 'Email']").click();
		driver.findElementByXPath("//input[@name = 'emailAddress']").sendKeys("testleaf811@yopmail.com");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		String name = driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a)[1]").getText();
		System.out.println("Verified the name "+name);
		Thread.sleep(2000);
		driver.findElementByXPath("(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a[@class='linktext'])[1]").click();
		driver.findElementByLinkText("Duplicate Lead").click();	
		String title = driver.getTitle();
		System.out.println(title);
		driver.findElementByXPath("//input[@name='submitButton']").click();
		String dupName = driver.findElementByXPath("//span[@id='viewLead_firstName_sp']").getText();
		System.out.println(dupName);
		if(name.equals(dupName)) {
			System.out.println("First Name " +name+ " and Duplicate Name " +dupName+" is verfied");
		}
		else {
			System.out.println("Both name are different");
		}
		driver.quit();


	}


}
