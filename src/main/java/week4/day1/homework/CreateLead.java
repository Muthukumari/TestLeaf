package week4.day1.homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");;
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("Amazon");
		driver.findElementById("createLeadForm_firstName").sendKeys("Muthu");
		driver.findElementById("createLeadForm_lastName").sendKeys("Kumari");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Muthukumari");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Murugan");
		WebElement source = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(source);
		dd.selectByValue("LEAD_COLDCALL");
		WebElement market = driver.findElementById("createLeadForm_marketingCampaignId");
		Select mar = new Select(market);
		mar.selectByVisibleText("Automobile");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("1234567890");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("test813@yopmail.com");
		driver.findElementByXPath("//input[@name='submitButton']").click();
		WebElement verifyName = driver.findElementByXPath("//span[@id='viewLead_firstNameLocal_sp']");
		String firstName = verifyName.getText();
		System.out.println(firstName);
		}


	}


