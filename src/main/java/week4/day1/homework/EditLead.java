package week4.day1.homework;

import org.openqa.selenium.chrome.ChromeDriver;

public class EditLead {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Find Leads").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Muthukumari");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		String title = driver.getTitle();
		System.out.println("The page title is "+ title);
		driver.findElementByLinkText("Edit").click();
		driver.findElementById("updateLeadForm_companyName").clear();
		driver.findElementById("updateLeadForm_companyName").sendKeys("Amazon");
		String text = driver.findElementById("updateLeadForm_companyName").getText();
		System.out.println(text);
		driver.findElementByXPath("//input[@name='submitButton']").click();
		String updatedText = driver.findElementById("viewLead_companyName_sp").getText();
		System.out.println(updatedText);
		if(updatedText.contains(text)) {
			System.out.println("Text is verfied successfully");
		}
		else {
			System.out.println("Text is not displayed propery");
		}
		driver.quit();
		

	}

}
