package week4.day1.homework;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead {

	public static void main(String[] args) throws InterruptedException, IOException {
		System.setProperty("webdriver.chrome.driver", "Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/control/main");
		driver.manage().window().maximize();
		driver.findElementById("username").sendKeys("DemoSalesManager");;
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Leads").click();
		driver.findElementByLinkText("Merge Leads").click();
		System.out.println(driver.getTitle());
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[1]").click();
		String parentWindow = driver.getWindowHandle();
		Set<String> fromLead = driver.getWindowHandles();
		System.out.println(fromLead.size());
		List<String> newWindow = new ArrayList<String>();
		newWindow.addAll(fromLead);
		String window = newWindow.get(2);
		driver.switchTo().window(window);
		String windowTitle = driver.getTitle();
		System.out.println(windowTitle);
		driver.findElementByName("id").sendKeys("10464");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		driver.switchTo().window(parentWindow);
		driver.findElementByXPath("(//img[@src='/images/fieldlookup.gif'])[2]").click();
		Set<String> toLead = driver.getWindowHandles();
		List<String> leadWin = new ArrayList<String>();
		leadWin.addAll(toLead);
		String windowLead = leadWin.get(2);
		driver.switchTo().window(windowLead);
		driver.findElementByName("id").sendKeys("10760");
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(2000);
		driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a").click();
		driver.switchTo().window(parentWindow);
		driver.findElementByXPath("//a[@class='buttonDangerous']").click();
		driver.switchTo().alert().accept();
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./Snaps/img.png");
		FileUtils.copyFile(source, dest);		
	}

}
