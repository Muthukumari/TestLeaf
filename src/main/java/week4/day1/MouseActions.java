package week4.day1;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class MouseActions {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://jqueryui.com/");
		driver.manage().window().maximize();
		driver.findElementByLinkText("Draggable").click();
		driver.switchTo().frame(driver.findElementByClassName("demo-frame"));
		Actions builder = new Actions(driver);
		WebElement drag = driver.findElementByXPath("//div[@id = 'draggable']/p");
		System.out.println(drag.getText());
		Thread.sleep(3000);
		builder.dragAndDropBy(drag, 100, 100).perform();
		
	}

}
