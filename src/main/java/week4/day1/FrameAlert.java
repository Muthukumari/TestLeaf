package week4.day1;

import org.openqa.selenium.chrome.ChromeDriver;

public class FrameAlert {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.w3schools.com/js/tryit.asp?filename=tryjs_prompt");
		driver.manage().window().maximize();
		
		//Frame
		driver.switchTo().frame("iframeResult");
		driver.findElementByXPath("//button[text() = 'Try it']").click();
		
		//alert
		driver.switchTo().alert().sendKeys("Muvish");
		driver.switchTo().alert().accept();
		String parentElement = driver.findElementById("demo").getText();
		System.out.println(parentElement);
		driver.close();
	}

}
