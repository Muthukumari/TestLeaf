package week4.day2;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC005_DeleteLead extends ProjectMethod{
	@BeforeClass(/*groups = "common"*/)
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription = "Create Lead";
		author = "Muthukumari";
		category = "Regression";
		excelFileName = "deleteLead";
		
	}
	
	@Test(/*groups = "regression"*/dataProvider="fetchdata")
	

	public void delete(String phonenumber) {
		WebElement leadLink = locateElement("LinkText", "Leads");
		click(leadLink);
		WebElement findLead = locateElement("LinkText", "Find Leads");
		click(findLead);
		WebElement phoneClick = locateElement("xpath", "//span[text()='Phone']");
		click(phoneClick);
		WebElement number = locateElement("xpath", "//input[@name = 'phoneNumber']");
		type(number, phonenumber);		
		WebElement findButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findButton);
		WebElement firstresultID = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']");
		String leadID = getText(firstresultID);
		WebElement firstID = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(firstID);
		WebElement delete = locateElement("LinkText", "Delete");
		click(delete);
		WebElement findLeadLink = locateElement("LinkText", "Find Leads");
		click(findLeadLink);
		WebElement enterID = locateElement("name", "id");
		type(enterID, leadID);
		System.out.println("Lead Id captured");
		WebElement findButton2 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findButton2);
		WebElement noRecords = locateElement("xpath", "//div[text()='No records to display']");
		System.out.println(getText(noRecords));
		verifyExactText(noRecords, "No records to display");
	}

}
