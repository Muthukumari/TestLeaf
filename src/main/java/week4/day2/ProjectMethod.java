package week4.day2;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import wdMethods.SeMethods;
import week6.day1.ExcelConvertDataProvider;

public class ProjectMethod extends SeMethods{



@DataProvider(name = "fetchdata")
public Object[][] getData() throws IOException {
	Object[][] excelData = ExcelConvertDataProvider.getData(excelFileName);
	return excelData;

}


@BeforeMethod(/*groups = "common"*/)
@Parameters({"browser" , "appUrl" , "userName", "password"})
	public void login(String browser,String appURL , String uName, String pass) {
		startApp(browser, appURL);
		WebElement username = locateElement("id", "username");
		type(username, uName);
		WebElement password = locateElement("id", "password");
		type(password, pass);
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		WebElement crmLink = locateElement("LinkText", "CRM/SFA");
		click(crmLink);


}
	@AfterMethod(/*groups = "common"*/)
	public void delete() {
	closeAllBrowsers();
	}
}
