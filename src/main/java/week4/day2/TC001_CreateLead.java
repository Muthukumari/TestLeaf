package week4.day2;


import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week6.day1.ExcelConvertDataProvider;
import week6.day1.ReadExcel;

public class TC001_CreateLead extends ProjectMethod {

	@BeforeClass(/*groups = "common"*/)
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription = "Create Lead";
		author = "Muthukumari";
		category = "Regression";
		excelFileName="CreateLead";

	}


	@Test(/*invocationCount = 2 , invocationTimeOut = 3000*/dataProvider = "fetchdata"
			/*groups = "smoke"*/)

	public void create(String cName , String fName, String lName, String emailadd, String ph) {

		WebElement leadButton = locateElement("LinkText", "Create Lead");
		click(leadButton);
		WebElement companyName = locateElement("id", "createLeadForm_companyName");
		type(companyName, cName);
		WebElement firstName = locateElement("id", "createLeadForm_firstName");
		type(firstName, fName);
		WebElement lastName = locateElement("id", "createLeadForm_lastName");
		type(lastName, lName);
		WebElement source = locateElement("id", "createLeadForm_dataSourceId");
		selectDropDownUsingText(source, "Conference");
		WebElement marketCamp = locateElement("id", "createLeadForm_marketingCampaignId");
		selectDropDownUsingIndex(marketCamp, 2);
		WebElement phone = locateElement("id", "createLeadForm_primaryPhoneNumber");
		type(phone, ph);
		WebElement email = locateElement("id", "createLeadForm_primaryEmail");
		type(email, emailadd);
		WebElement createLead = locateElement("class", "smallSubmit");
		click(createLead);
		WebElement verifyText = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(verifyText, "Muthukumari");

	}
	
}

