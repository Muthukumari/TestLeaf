package week4.day2;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC002_EditLead extends ProjectMethod{
	
	@BeforeClass(/*groups = "common"*/)
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription = "Create Lead";
		author = "Muthukumari";
		category = "Regression";
		excelFileName="EditLead";
		
	}
	

	@Test(/*dependsOnMethods ="week4.day2.TC001_CreateLead.create"*//*groups = "sanity"*/dataProvider="fetchdata")

	public void edit(String fName,String CompName) {
		WebElement leadLink = locateElement("LinkText", "Leads");
		click(leadLink);
		WebElement findLead = locateElement("LinkText", "Find Leads");
		click(findLead);
		WebElement firstName = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(firstName, fName);
		WebElement findButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findButton);
		WebElement firstLink = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(firstLink);
		String title = driver.getTitle();	
		verifyTitle(title);
		WebElement edit = locateElement("LinkText", "Edit");
		click(edit);
		locateElement("id", "updateLeadForm_companyName").clear();
		WebElement companyname = locateElement("id", "updateLeadForm_companyName");
		type(companyname, CompName);
		WebElement update = locateElement("xpath", "//input[@name='submitButton']");
		click(update);
		WebElement text1 = locateElement("id", "viewLead_companyName_sp");
		verifyExactText(text1, CompName);
		//closeBrowser();

	}

}
