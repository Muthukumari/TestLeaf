package week4.day2;



import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC003_MergeLead extends ProjectMethod{
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription = "Create Lead";
		author = "Muthukumari";
		category = "Regression";
		
	}
	@Test(enabled = false)
	
	public void mergeLead() {
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement username = locateElement("id", "username");
		type(username, "DemoSalesManager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		WebElement crmLink = locateElement("LinkText", "CRM/SFA");
		click(crmLink);
		WebElement leadLink = locateElement("LinkText", "Leads");
		click(leadLink);
		WebElement mergeLink = locateElement("LinkText", "Merge Leads");
		click(mergeLink);
		WebElement image = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[1]");
		click(image);
		switchToWindow(2);
		WebElement leadId = locateElement("name", "id");
		type(leadId, "10464");
		WebElement findButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findButton);
		WebElement firstValue = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		clickWithNoSnap(firstValue);
		switchToWindow(0);
		WebElement image2 = locateElement("xpath", "(//img[@src='/images/fieldlookup.gif'])[2]");
		click(image2);
		WebElement secondLead = locateElement("name", "id");
		type(secondLead, "10460");
		WebElement findButton1 = locateElement("xpath", "//button[text()='Find Leads']");
		click(findButton1);
		WebElement Value = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(Value);
		WebElement mergeLead = locateElement("LinkText", "Merge");
		click(mergeLead);
		acceptAlert();
		takeSnap();
		closeAllBrowsers();
		
		
		
		
		
	}

}
