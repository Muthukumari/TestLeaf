package week4.day2;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC004_DuplicateLead extends ProjectMethod {
	

	@BeforeClass
	public void setData() {
		testCaseName = "TC001";
		testCaseDescription = "Create Lead";
		author = "Muthukumari";
		category = "Regression";
		
	}
@Test(dependsOnMethods = "week4.day2.TC002_EditLead.edit")
	public void duplicateLead() {
		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
		WebElement username = locateElement("id", "username");
		type(username, "DemoSalesManager");
		WebElement password = locateElement("id", "password");
		type(password, "crmsfa");
		WebElement login = locateElement("class", "decorativeSubmit");
		click(login);
		WebElement crmLink = locateElement("LinkText", "CRM/SFA");
		click(crmLink);
		WebElement leadLink = locateElement("LinkText", "Leads");
		click(leadLink);
		WebElement findLead = locateElement("LinkText", "Find Leads");
		click(findLead);
		WebElement emailLink = locateElement("xpath", "//span[text() = 'Email']");
		click(emailLink);
		WebElement emailField = locateElement("xpath", "//input[@name = 'emailAddress']");
		type(emailField, "testleaf818@yopmail.com");
		WebElement findButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findButton);
		WebElement text = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a");
		verifyExactText(text, "Muthukumari");
		WebElement firstID = locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a");
		click(firstID);
		WebElement duplicateLink = locateElement("LinkText", "Duplicate Lead");
		click(duplicateLink);
		verifyTitle("Duplicate Lead | opentaps CRM");
		WebElement createButton = locateElement("xpath", "//input[@value='Create Lead']");
		click(createButton);
		WebElement duplicateFirstName = locateElement("id", "viewLead_firstName_sp");
		verifyExactText(duplicateFirstName, "Muthukumari");
		closeBrowser();
	}

}
