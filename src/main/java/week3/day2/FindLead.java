package week3.day2;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class FindLead {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.manage().window().maximize();
		driver.findElementByXPath("//input[@id = 'username']").sendKeys("DemoSalesManager");
		driver.findElementByXPath("//input[@id = 'password']").sendKeys("crmsfa");
		driver.findElementByXPath("//input[@class = 'decorativeSubmit']").click();
		driver.findElementByXPath("//a[contains(text(),'CRM/SFA')]").click();
		driver.findElementByXPath("//a[contains(text(),'Leads')]").click();
		driver.findElementByXPath("//a[contains(text(),'Find Leads')]").click();
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys("Sabitha");
		driver.findElementByXPath("//button[contains(text(),'Find Leads')]").click();
		WebDriverWait wait = new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Find Leads')]")));
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		File source = driver.getScreenshotAs(OutputType.FILE);
		File dest = new File("./Snaps/img.png");
		FileUtils.copyFile(source, dest);
		

	}

}
