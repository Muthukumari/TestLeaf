package week3.homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class AllElementsCreateLead {

	public static void main(String[] args) {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("http://leaftaps.com/opentaps/");
		driver.findElementById("username").sendKeys("DemoSalesManager");
		driver.findElementById("password").sendKeys("crmsfa");
		driver.findElementByClassName("decorativeSubmit").click();
		driver.findElementByLinkText("CRM/SFA").click();
		driver.findElementByLinkText("Create Lead").click();
		driver.findElementById("createLeadForm_companyName").sendKeys("CTS");
		driver.findElementById("createLeadForm_firstName").sendKeys("Muthukumari");
		driver.findElementById("createLeadForm_lastName").sendKeys("Murugan");
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		Select dd = new Select(src);
		dd.selectByVisibleText("Conference");
		WebElement marketing = driver.findElementById("createLeadForm_marketingCampaignId");
		Select dropdown = new Select(marketing);
		dropdown.selectByValue("CATRQ_CARNDRIVER");
		driver.findElementById("createLeadForm_parentPartyId").sendKeys("10476");
		driver.findElementById("createLeadForm_firstNameLocal").sendKeys("Muthu");
		driver.findElementById("createLeadForm_lastNameLocal").sendKeys("Murugan");
		driver.findElementById("createLeadForm_personalTitle").sendKeys("Greetings");
		driver.findElementById("createLeadForm_generalProfTitle").sendKeys("Form Submission");
		driver.findElementById("createLeadForm_departmentName").sendKeys("Finance");
		driver.findElementById("createLeadForm_annualRevenue").sendKeys("120000");
		WebElement currency = driver.findElementById("createLeadForm_currencyUomId");
		Select dp = new Select(currency);
		dp.selectByValue("INR");
		WebElement industry = driver.findElementById("createLeadForm_industryEnumId");
		Select dr = new Select(industry);
		dr.selectByIndex(8);
		driver.findElementById("createLeadForm_numberEmployees").sendKeys("120");
		WebElement ownership = driver.findElementById("createLeadForm_ownershipEnumId");
		Select owner = new Select(ownership);
		owner.selectByVisibleText("Partnership");
		driver.findElementById("createLeadForm_sicCode").sendKeys("123");
		driver.findElementById("createLeadForm_tickerSymbol").sendKeys("Diamond");
		driver.findElementById("createLeadForm_description").sendKeys("Testing Purpose");
		driver.findElementById("createLeadForm_importantNote").sendKeys("Important");
		driver.findElementById("createLeadForm_primaryPhoneCountryCode").sendKeys("91");
		driver.findElementById("createLeadForm_primaryPhoneAreaCode").sendKeys("044");
		driver.findElementById("createLeadForm_primaryPhoneExtension").sendKeys("222");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("test@yopmail.com");
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys("1234567890");
		driver.findElementById("createLeadForm_primaryPhoneAskForName").sendKeys("Raja");
		driver.findElementById("createLeadForm_primaryWebUrl").sendKeys("www.google.com");
		driver.findElementById("createLeadForm_generalToName").sendKeys("Bala");	
		driver.findElementById("createLeadForm_generalAttnName").sendKeys("Divya");
		driver.findElementById("createLeadForm_generalAddress1").sendKeys("Address");
		driver.findElementById("createLeadForm_generalAddress2").sendKeys("Address12");
		driver.findElementById("createLeadForm_generalCity").sendKeys("Chennai");
		driver.findElementById("createLeadForm_generalPostalCode").sendKeys("601301");
		driver.findElementById("createLeadForm_generalPostalCodeExt").sendKeys("600");
		WebElement country = driver.findElementById("createLeadForm_generalCountryGeoId");
		Select ccdd = new Select(country);
		ccdd.selectByValue("IND");
		try {
			Thread.sleep(300);
		} catch (InterruptedException e) {
			System.out.println("TamilNadu is not found");
		}
		WebElement State = driver.findElementById("createLeadForm_generalStateProvinceGeoId");
		Select statedd = new Select(State);
		statedd.selectByVisibleText("TAMILNADU");
		driver.findElementByClassName("smallSubmit").click();
		
		
		
		
		
		


	}

}
