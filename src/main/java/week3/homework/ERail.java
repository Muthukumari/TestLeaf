package week3.homework;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ERail {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://erail.in/");
		driver.manage().window().maximize();
		driver.findElementById("txtStationFrom").clear();
		driver.findElementById("txtStationFrom").sendKeys("MAS",Keys.TAB);
		driver.findElementById("txtStationTo").clear();
		driver.findElementById("txtStationTo").sendKeys("SBC",Keys.TAB);
		boolean selected = driver.findElementById("chkSelectDateOnly").isSelected();
		if(selected) {
			driver.findElementById("chkSelectDateOnly").click();
		}
		Thread.sleep(3000);
		WebElement table = driver.findElementByXPath("//table[@class='DataTable TrainList']");
		List<WebElement> rows = table.findElements(By.tagName("tr"));
		System.out.println(rows.size());
		WebElement tr = rows.get(1);
		for (WebElement eachValue : rows) {
			String value =eachValue.findElements(By.tagName("td")).get(1).getText();
			System.out.println(value);
		}
		List<WebElement> columns = tr.findElements(By.tagName("td"));
		System.out.println(columns.size());
		String text = columns.get(1).getText();
		System.out.println(text);
	}

}
