package week3.homework;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class IrctcLogin {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./Drivers\\chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.irctc.co.in/nget/train-search");
		Thread.sleep(3000);
		driver.manage().window().maximize();
		driver.findElementByXPath("//span[text()='AGENT LOGIN']").click();
		driver.findElementByLinkText("Sign up").click();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Muthu");
		driver.findElementById("userRegistrationForm:password").sendKeys("kumari");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("kumari");
		WebElement Security = driver.findElementById("userRegistrationForm:securityQ");
		Select dd = new Select(Security);
		dd.selectByValue("4");
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("Test");
		WebElement language = driver.findElementById("userRegistrationForm:prelan");
		Select dr = new Select(language);
		dr.selectByVisibleText("English");
		driver.findElementById("userRegistrationForm:firstName").sendKeys("First");
		driver.findElementById("userRegistrationForm:middleName").sendKeys("Middle");
		driver.findElementById("userRegistrationForm:lastName").sendKeys("Last");
		WebElement occupation = driver.findElementById("userRegistrationForm:occupation");
		Select dp = new Select(occupation);
		dp.selectByIndex(3);
		driver.findElementById("userRegistrationForm:uidno").sendKeys("2314569875");
		driver.findElementById("userRegistrationForm:idno").sendKeys("ABCDE456L");
		WebElement country = driver.findElementById("userRegistrationForm:countries");
		Select co = new Select(country);
		co.selectByVisibleText("India");
		Thread.sleep(3000);
		driver.findElementById("userRegistrationForm:email").sendKeys("test@yopmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("1234567890");
		WebElement nationality = driver.findElementById("userRegistrationForm:nationalityId");
		Select nat = new Select(nationality);
		nat.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("sri nagar 3rd street");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("601301");
		Thread.sleep(3000);
		WebElement city = driver.findElementById("userRegistrationForm:cityName");
		Select cc = new Select(city);
		cc.selectByVisibleText("Kanchipuram");
		WebElement post = driver.findElementById("userRegistrationForm:postofficeName");
		Select po = new Select(post);
		po.selectByVisibleText("Padappai S.O");
		driver.findElementById("userRegistrationForm:landline").sendKeys("1234568975");
				
		
		}

}
