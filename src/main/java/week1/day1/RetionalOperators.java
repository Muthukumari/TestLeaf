package week1.day1;

public class RetionalOperators {

	public static void main(String[] args) {
		int i = 10;
		int j = 5;
		System.out.println("Greatest of two numbers is: " + (i > j));
		System.out.println("Smallest of two numbers is: " + (i < j));
		System.out.println("Greater than or equal to two numbers is: " + (i >= j));
		System.out.println("Smaller than or equal to two numbers is: " + (i <= j));
		System.out.println("Two numbers are equal: " + (i == j));
		System.out.println("First number is equal to second number: " + (i != j));

	}

}
