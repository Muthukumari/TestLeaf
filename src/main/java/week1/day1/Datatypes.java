package week1.day1;

public class Datatypes {

	public static void main(String[] args) {
		int i = 20;
		System.out.println("The value is i is: " + i);
		float f = 10500.25f;
		System.out.println("The value is f is: " + f);
		double d = 797997979;
		System.out.println("The value is d is: " + d);
		long l = 7894561230l;
		System.out.println("The value is l is: " + l);
		short s = 123;
		System.out.println("The value is s is: " + s);
		byte b = 14;
		System.out.println("The value is b is: " + b);
		char c = 'Y';
		System.out.println("The value is c is: " + c);
		boolean bo = true;
		System.out.println("The value is bo is: " + bo);
		
		
				

	}

}
