package week1.codechallenge1;

import java.util.Scanner;

public class ReverseNumber {

	public static void main(String[] args) {
		System.out.println("Reverse of a given number");
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int temp = n, sum = 0, r;
		while(n>0)
		{
			r = n%10;
			sum = (sum * 10)+r;
			n = n/10;
		}
		System.out.println("Reverse of the given number is: " + sum);
		}

			
		}


