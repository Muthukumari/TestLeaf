package week1.codechallenge1;

import java.util.Scanner;

public class FactorialNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	System.out.println("Enter the factorial number");
		int i = sc.nextInt();
		int fact = 1;
		for(int j = 1; j<= i; j++) {
			 fact = fact * j;
		}
		System.out.print("The Factorial of given number is " + i + " = " + fact);
	}
	

}
