package week1.day2;

public class DatatypesToArray {

	public static void main(String[] args) {
		//int datatype
		int i[] = {1,2,3,5};
		for(int a: i) {
			System.out.println(a);
		}
		//String datatype
		String strArray[] = {"one", "two"};
		for(String theString: strArray) {
			System.out.println(theString);
		}
		//double datatype
		double d[] = {102.56,25698.236,1689.1245,24.65};
		for(double data: d)
		{
			System.out.println(data);
		}
		//long datatype
		long l[] = {12358988454l,579864646l,54895956l,123265l};
		for(long value: l)
		{
			System.out.println(value);
		}
		//float datatype
				float f[] = {1234f,7578,54895956,123265};
				for(float value: f)
				{
					System.out.println(value);
				}
				//short datatype
				short s[] = {235,578,7890};
				for(short value: s)
				{
					System.out.println(value);
				}
				//byte datatype
				byte b[] = {123,100,127,12};
				for(byte value: b)
				{
					System.out.println(value);
				}
				//char datatype
				char c[] = {'M', 'U', 'V', 'I','S','H'};
				for(char value: c)
				{
					System.out.println(value);
				}
				//boolean datatype
				boolean bo [] = {true};
				for(boolean value: bo)
				{
					System.out.println(value);
				}
				}

}
