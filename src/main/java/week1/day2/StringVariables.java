package week1.day2;

public class StringVariables {

	public static void main(String[] args) {
		//length 
		String name = new String(" Muthu kumari ");
		System.out.println(name.length());
		//concat
		String concat = "Chennai";
		System.out.println(name.concat(" " + concat));
		//char finding
		char allChar[] = name.toCharArray();
		for(char eachchar : allChar)
		System.out.println(eachchar);
		//checkchar
		boolean checkChar = name.contains("w");
		System.out.println(checkChar);
		//String to String
		String object = name.toString();
		System.out.println(object);
		//String to UpperCase
		String upperCase = name.toUpperCase();
		System.out.println(upperCase);
		//to trim the whitespace
		String trim = name.trim();
		System.out.println(trim);
		//to split
		String allWords[] = name.split("u");
		for(String split: allWords)
			System.out.println(split);
		//String valueof
		double d = 1025.235;
		boolean b = true;
		char[] rename =  {'M', 'u','t', 'h', 'u'};
		System.out.println(String.valueOf(rename));
		System.out.println(String.valueOf(d));
		System.out.println(String.valueOf(b));
		//substring
		String subString = name.substring(5);
		System.out.println(subString);
		String subString1 = name.substring(3,10);
		System.out.println(subString1);
		//startswith
		boolean String = name.startsWith(" ");
		System.out.println(String);
		boolean String1 = name.startsWith(" ",  0);
		System.out.println(String1);
		
	
		
	}

}
