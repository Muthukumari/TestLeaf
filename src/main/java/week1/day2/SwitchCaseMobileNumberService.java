package week1.day2;

import java.util.Scanner;

public class SwitchCaseMobileNumberService {

	public static void main(String[] args) {
		System.out.println("Enter the service number");
		Scanner sc = new Scanner(System.in);
		int a = sc.nextInt();
		switch(a) {
		case 944:
		{
			System.out.println("BSNL");
			break;
		}
		case 900:
		{
			System.out.println("Airtel");
			break;
		}
		case 897:
		{
			System.out.println("Idea");
			break;
		}
		case 630:
		{
			System.out.println("Jio");
			break;
		}
		default:
		{
			System.out.println("Invalid Service number");
			break;
		}
		}
		

	}

}
