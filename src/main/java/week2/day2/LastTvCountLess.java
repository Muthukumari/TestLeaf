package week2.day2;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class LastTvCountLess {

	public static void main(String[] args) {
		Map<String,Integer> model = new LinkedHashMap<String,Integer>();
		model.put("Onida", 3);
		model.put("Philips", 2);
		model.put("LG", 55);
		
		Set<String> allTvs = model.keySet();
		List<String> tvs = new ArrayList<String>();
		tvs.addAll(allTvs);
		System.out.println(tvs.get(tvs.size()-1));
		Integer count = model.get(tvs.get(tvs.size()-1));
		System.out.println(count);
		model.put(tvs.get(tvs.size()-1),count - 1);
		System.out.println(count);
		
		
	}

}
