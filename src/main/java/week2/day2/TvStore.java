package week2.day2;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

public class TvStore {
	public static void main(String[] args) {
		Map<String,Integer> allTvs = new LinkedHashMap<String,Integer>();
		allTvs.put("Samsung", 5);
		allTvs.put("Philips", 2);
		allTvs.put("Onida", 3);
		int total = 0;
		for (Entry<String, Integer> string : allTvs.entrySet()) {
			System.out.println(string.getValue());
			total = total + string.getValue();

		}
		System.out.println(total);
	}

}
