package week2.day2;

public class ThrowsException {

	public static void main(String[] args) throws Exception {
		ThrowsException obj = new ThrowsException();
		try {
		obj.ageCal(5);
		}
		catch (InvalidAge e) {
			System.out.println("Invalid Age");
		}

	}
	public void ageCal(int age) throws Exception {
		if(age<18) {
			throw new InvalidAge();
		}
	else
	{
		System.out.println("Valid age");
	}

}
}