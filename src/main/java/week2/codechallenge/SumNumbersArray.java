package week2.codechallenge;

import java.util.Scanner;

public class SumNumbersArray {

	public static void main(String[] args) {
		int sum = 0;
	Scanner sc = new Scanner(System.in);
	System.out.println("Enter the numbers");
	int no = sc.nextInt();
	int arr[] = new int[no];
	System.out.println("Enter the array numbers");
	for(int i = 0;i<no;i++)
	{
		arr[i] = sc.nextInt();
	}
	
	for (int i : arr) {
		sum = sum+i;
	}	
	System.out.println(sum);
	}

}
