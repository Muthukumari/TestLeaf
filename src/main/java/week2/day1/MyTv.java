package week2.day1;

public class MyTv extends Media {
	
	public void browsing()
	{
		System.out.println("Browsing");
	}

	public static void main(String[] args) {
		Samsung myTv = new Samsung();
		myTv.changeChannel(75);
		myTv.changeChannel("Sun");
		myTv.increaseVolume('+');
		myTv.power(true);
		MyTv Object = new MyTv();
		Object.browsing();
	
	}

}
