package week2.day1;

public interface Tv {
	
	public void changeChannel(int channelNumber);
	
	public void changeChannel(String name);
	
	public void increaseVolume(char sign);
	
	public void power(boolean off);
	

}
