package week2.day1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ElectronicShop {

	public static void main(String[] args) {
		List<String> tv = new ArrayList<String>();
		tv.add("Samsung");
		tv.add("Panasonic");
		tv.add("Samsung");
		tv.add("Onida");
		tv.add("Philips");
		int size = tv.size();
		System.out.println(size);
		for(String each: tv) {
			System.out.println(each);
		}
		tv.remove(4);
		System.out.println("Removed the TV");
		for(String each: tv)
		{
			System.out.println(each);
		}
		Collections.sort(tv);
		System.out.println("Collections is displayed");
		for(String each: tv) {
		System.out.println(each);
	}
	}

}
