package week2.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class TvShop {

	public static void main(String[] args) {
		Set<String> tvModels = new TreeSet<String>();
		tvModels.add("Samsung");
		tvModels.add("Onida");
		tvModels.add("Philips");
		tvModels.add("Onida");
		tvModels.add("BPL");
		for(String each: tvModels) {
			System.out.println(each);
		}
		List<String> lastTv = new ArrayList<String>();
		lastTv.addAll(tvModels);
		System.out.println("Last TV model is: ");
		System.out.println(lastTv.get(3));
		
		
		

	}

}
