package week2.day1;

public class Samsung implements Tv {


	@Override
	public void changeChannel(int channelNumber) {
		System.out.println("Channel Changed " + channelNumber );
		
	}

	@Override
	public void changeChannel(String name) {
		System.out.println("Channel changed with name " + name );
		
	}

	@Override
	public void increaseVolume(char sign) {
	System.out.println("Increased the volume " + sign);
		
	}

	@Override
	public void power(boolean off) {
		System.out.println("Power ON :" + off);
		
	}
	
	public static void main(String[] args) {
	Samsung myTv = new Samsung();
	myTv.changeChannel(78);
	
}
	}

