package projects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.xmlbeans.impl.xb.xsdschema.impl.ListDocumentImpl.ListImpl;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import wdMethods.SeMethods;
import week4.day2.ProjectMethod;

public class ZoomCarChennai extends SeMethods{
	@BeforeClass
	public void setData() {
		testCaseName = "ZoomCar";
		testCaseDescription = "Create Lead";
		author = "Muthukumari";
		category = "Function";

	}
	@Test
	public void searchCar() {
		startApp("Chrome", "https://www.zoomcar.com/chennai/");

		//Start Journey SearchBar click
		WebElement clickJourney = locateElement("class", "search");
		click(clickJourney);

		//Click on Popular Point
		WebElement popularPoint = locateElement("xpath", "(//div[@class='items'])[2]");
		click(popularPoint);

		//Clicking the next button
		WebElement nextButton = locateElement("class", "proceed");
		click(nextButton);

		//Date selection
		WebElement dateSelection = locateElement("xpath", "//div[@class='day']");
		click(dateSelection);
		webDriverWait(dateSelection);

		//Next button click
		WebElement nextButtonInDate = locateElement("class", "proceed");
		webDriverWait(nextButtonInDate);
		click(nextButtonInDate);
		

		//Confirm Start Date
		WebElement confirmStartDate = locateElement("xpath", "//div[@class='day picked ']");
		webDriverWait(confirmStartDate);
		verifyPartialText(confirmStartDate, "21");

		//Clicking on Done button
		WebElement doneButton = locateElement("class", "proceed");
		click(doneButton);
		webDriverWait(doneButton);

		//Book Now Car list
		List<WebElement> listofcars = driver.findElementsByXPath("//div[@class='car-item']");
		System.out.println(listofcars.size());
		
		List<WebElement> listofprice = driver.findElementsByXPath("//div[@class='price']");
		List<String> price = new ArrayList<String>();
		for (WebElement eachPrice : listofprice) {
			System.out.println("List of price is "+ eachPrice.getText().replaceAll("[^a-zA-Z0-9]", ""));
			price.add(eachPrice.getText().replaceAll("[^a-zA-Z0-9]", ""));
		}
		String max = Collections.max(price);
		System.out.println("The maximum price is " +max);
		
		//driver.findElementByXPath("//div[contains(text()," +max+")]/preceeding::h3";
		
		

}

}
