package week6.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelConvertDataProvider  {

	//Changing the main method to static
	public static Object[][] getData (String fileName) throws IOException {

		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+fileName+ ".xlsx");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int lastRowNum = sheet.getLastRowNum();
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		//Creating 2d array
		Object [][] data = new Object[lastRowNum][lastCellNum];
		for(int i = 1;i <=lastRowNum;i++) {
			XSSFRow row = sheet.getRow(i);
			for (int j = 0; j < lastCellNum; j++) {
				XSSFCell cell = row.getCell(j);
				String stringCellValue = cell.getStringCellValue();
				//Assigning the value
				data[i-1][j]=stringCellValue;
				System.out.println(stringCellValue);
			}

			wbook.close();

		}
		return data;

	}
}