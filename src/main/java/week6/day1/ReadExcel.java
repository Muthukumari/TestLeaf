package week6.day1;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReadExcel {

	public static void main(String[] args) throws IOException {
		//declaring the workbook path
		XSSFWorkbook wbook = new XSSFWorkbook("./data/CreateLead.xlsx");
		
		//declaring the sheet name
		XSSFSheet sheet = wbook.getSheet("CreateLead");
		
		//counting the number of rows and iterating
		int lastRowNum = sheet.getLastRowNum();
		for(int i = 1;i <=lastRowNum;i++) {
		XSSFRow row = sheet.getRow(i);
		
		//counting the number of column an iterating
		int lastCellNum = sheet.getRow(0).getLastCellNum();
		for (int j = 0; j < lastCellNum; j++) {
			XSSFCell cell = row.getCell(j);
			String stringCellValue = cell.getStringCellValue();
			System.out.println(stringCellValue);
		}
		
		wbook.close();
		}

	}

}
